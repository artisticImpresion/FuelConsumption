/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.training.fuelconsumption;

import java.sql.SQLException;
import java.util.InputMismatchException;

/**
 *
 * @author artisticImpresion
 */
public class AppStarter {

	public static void main(String[] args) throws SQLException {

		FuelConsumptionApp app = new FuelConsumptionApp();
		Integer result = null;
		Integer error = -1;

		// TODO
		boolean quit = false;
		while (!quit) {
			System.out.println("0 - pokaż opcje");
			System.out.print("~");

			int choice;
			try {
				choice = Utils.enter.nextInt();

			} catch (InputMismatchException e) {
				System.out.print("Wybrana opcja musi być liczbą! ");
				choice = 0;
			}
			Utils.enter.nextLine();

			switch (choice) {
			case 0:
				printActions();
				break;
			case 1:
				quit = true;
				System.out.println("Bye!");
				break;
			case 2:
				System.out.println("Starting...");
				result = app.startApp();
				break;
			case 3:
				System.out.println("Starting DB API...");
				app.startApp("db mode");
				break;
			case 4:
				System.out.println("Starting DB API in repair mode...");
				app.startApp("db repair mode");
				break;
			// case 5:
			// // noOption
			// break;
			default:
				System.out.println("Dokonaj wyboru!");
				break;
			}

		}

		if (result.equals(error)) {
			System.out.println("Wystąpił błąd! Spróbuj ponownie uruchomić aplikację...");
		} else {
			System.out.println("Do zobaczenia!");
		}
	}

	public static void printActions() {
		System.out.println("Wybierz opcję:");

		System.out.println("1 - wyjdz z aplikacji");
		System.out.println("2 - uruchom FuelConsumption App");
		System.out.println("3 - uruchom API bazy danych");
	}
}
