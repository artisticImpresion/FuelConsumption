/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.training.fuelconsumption;

import java.util.ArrayList;

/**
 *
 * @author artisticImpresion
 */
public class Display {

	public void showRegistry(ArrayList<String> aList) {
		if (aList.isEmpty()) {
			System.out.println("Lista nie zawiera elementów!");
		} else {

			for (int i = 0; i <= (aList.size()) - 1; i++) {
				String listElem = aList.get(i);
				System.out.println(listElem);
			}
			System.out.println(".");

		}
	}

	public void println(String textToShow) {
		System.out.println(textToShow);
	}

	public void print(String textToShow) {
		System.out.print(textToShow);
	}
}
