/**
 * 
 */
package dev.training.fuelconsumption;

/**
 * @author artisticImpresion
 *
 */
public class DisplayConstants {
  
	public static final String HUNDRED = "100";
  public static final String LABEL_INPUT_CURRENT_MILEAGE = "Podaj aktualny stan licznika kilometrów: ";
	public static final String LABEL_INPUT_FUEL_AMOUNT = "Podaj ilość zatankowanego paliwa (np. 12,38 l): ";
	public static final String LABEL_INPUT_LAST_MILEAGE = "Podaj poprzedni stan licznika: ";
	public static final String LABEL_RADIO_ASK_FOR_LAST_MILEAGE = "Czy chcesz podać poprzedni stan?";
	public static final String MESSAGE_ERROR_NO_LAST_MILEAGE = "Brak poprzedniego stanu licznika utrudnia działanie aplikacji.";
	public static final String MESSAGE_INFO_CURRENT_MILEAGE = "Podany stan licznika: ";
	public static final String MESSAGE_INFO_FUEL_USE_A = "Od poprzedniego tankowania samochód miał spalanie ";
	public static final String MESSAGE_INFO_FUEL_USE_B = " litra na 100km.";
	public static final String MESSAGE_INFO_LAST_REFUELING_A = "Ostatnio zatankowałeś: ";
	public static final String MESSAGE_INFO_LAST_REFUELING_B = " litrów paliwa.";
	public static final String MESSAGE_INFO_LAST_MILEAGE = "Poprzednio zarejestrowany stan licznika: ";
	public static final String MESSAGE_INFO_NO_LAST_MILEAGE = "Nie masz jeszcze zapisanego poprzedniego stanu licznika.";
	public static final String MESSAGE_WARNING_MILEAGE_NOT_WHOLE_NUMBER = "Stan licznika musi być liczbą całkowitą!";
	public static final String INFO_MESSAGE_AVG_DISTANCE = "Średnia pokonanej odległości na jednym baku: ";
	public static final String INFO_MESSAGE_AVG_FUEL_AMOUNT = "Średnia ilość tankowanego paliwa: ";
	public static final String INFO_MESSAGE_AVG_FUEL_CONSUMPTION = "Średnie zużycie paliwa: ";
	public static final String INFO_MESSAGE_EMPTY_TABLE = "[pusta]";
	public static final String INFO_MESSAGE_FUEL_CONSUMPTION = "Zużycie paliwa: ";
	public static final String INFO_MESSAGE_TABLE = "Tabela: ";
	public static final String PIPE = "|";
	public static final String SLASH = "/";
	public static final String SPACE = " ";
	public static final String UNIT_LITER_L = " l";
	public static final String UNIT_KILOMETER_KM = " km";
	public static final String VALIDATION_INPUT_NULL_CURRENT_MILEAGE = "Stan licznika niepoprawny!";
	
}
