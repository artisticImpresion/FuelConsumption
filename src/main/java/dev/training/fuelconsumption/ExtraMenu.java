/**
 * 
 */
package dev.training.fuelconsumption;

/**
 * @author artisticImpresion
 *
 */
public class ExtraMenu {
	private String command;
	private String EMPTY = "";

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}
	
	private int readCommand(String input) {
		if (!EMPTY.equals(input)) {
			return 1;
		}
		return -1;
	}
	
	private boolean isCommandEmpty (int readResult) {
		if (readResult == -1) {
			return true;
		} else {
			return false;
		}
	}
	
	public String enterCommand() {
		System.out.print("~");
		String input = Utils.enter.next();
		int check = readCommand(input);
		if(isCommandEmpty(check)) {
			return null;
		} else {
			return "...";
		}
	}
}
