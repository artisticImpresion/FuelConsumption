/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.training.fuelconsumption;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author artisticImpresion
 */
public class FileRead {
	private static final Logger LOGGER = Logger.getLogger(FileRead.class.getName());
	private String filePath = "db.txt";

	public String getFilePath() {
		return this.filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	// public String readLast() {
	//
	// try {
	// File file = new File(getFilePath());
	// BufferedReader reader = new BufferedReader(new FileReader(file));
	// String readLine = reader.readLine();
	// reader.close();
	//
	// return readLine;
	// } catch (IOException ex) {
	// Logger.getLogger(FileRead.class.getName()).log(Level.SEVERE, null, ex);
	// }
	// return "0";
	// }
	//
	// public int readLines() {
	// try {
	// File file = new File(getFilePath());
	// BufferedReader reader = new BufferedReader(new FileReader(file));
	// int linesNum = 0;
	// String lines;
	// while ((lines = reader.readLine()) != null) {
	// linesNum++;
	// }
	// reader.close();
	//
	// return linesNum;
	// } catch (IOException ex) {
	// Logger.getLogger(FileRead.class.getName()).log(Level.SEVERE, null, ex);
	// }
	// return -1;
	// }

	public ArrayList<String> readList() {
		ArrayList<String> rowsList = new ArrayList<>();

		try {
			File file = new File(getFilePath());
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String lineContent = null;
			while ((lineContent = reader.readLine()) != null) {
				rowsList.add(lineContent);
			}

			reader.close();
		} catch (IOException ex) {
			LOGGER.log(Level.SEVERE, ex.toString(), ex);
			// Logger.getLogger(FileRead.class.getName()).log(Level.SEVERE, null, ex);
		}
		return rowsList;
	}

	/*
	 * temporary to test for each loop;
	 */
	public void listAllLines(ArrayList<String> prevMileage) {
		for (String elem : prevMileage)
			System.out.println(elem); // size();
	}
}
