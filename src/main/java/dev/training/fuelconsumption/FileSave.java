package dev.training.fuelconsumption;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author artisticImpresion
 **/
public class FileSave {

	public void saveCurrentMileage(String mileage) throws FileNotFoundException {
		try {
			FileWriter file = new FileWriter("db.txt", true);
			BufferedWriter saveFile = new BufferedWriter(file);
			saveFile.write(mileage);
			saveFile.newLine();
			saveFile.close();
		} catch (IOException ex) {
			Logger.getLogger(FileRead.class.getName()).log(Level.SEVERE, null, ex);
		}
		/* testing */
	}

}
