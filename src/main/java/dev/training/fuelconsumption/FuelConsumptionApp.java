/**
 * 
 */
package dev.training.fuelconsumption;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import dev.training.fuelconsumption.model.DbConstants;
import dev.training.fuelconsumption.model.FuelConsumptionDb;
import dev.training.fuelconsumption.model.Mileage;

/**
 * @author artisticImpresion
 *
 */
public class FuelConsumptionApp {
  private boolean replyYes = false;
  private Integer appOk = 0;
  private Integer appError = -1;

  public boolean isReplyYes() {
    return replyYes;
  }

  public void setReplyYes(boolean replyYes) {
    this.replyYes = replyYes;
  }

  public Integer getAppOk() {
    return appOk;
  }

  public Integer getAppError() {
    return appError;
  }

  public Integer startApp() {
    // FileRead lastMileage = new FileRead();
    // System.out.println(lastMileage.readLines()); //counts a number of lines in
    // file and displays it

    // Display listMileage = new Display();

    String lastMileage = lastMileage();
    Integer tempFirstMileage = null;
    Integer testTemp = null;
    if (lastMileage == null) {
      tempFirstMileage = getLastMileageFromUser(lastMileage, tempFirstMileage);

      if (testTemp == tempFirstMileage) {
        return getAppError();
      } else {
        saveDataToTextFile(tempFirstMileage.toString());
      }

      if (lastMileage == null) {
        lastMileage = lastMileage();
      }
    }
    Utils.br("Poprzednio zapisany stan licznika: " + lastMileage);
    Utils.br();

    // TODO wypisanie listy stanów licznika
    // listMileage.showRegistry(test.readList());
    // FileRead test2 = new FileRead();
    // test2.listAllLines(test.readList()); //displays a list of all lines in file
    // lastMileage.readLast();
    // temporary there isn't last line instead it's first line of db.txt file
    // System.out.println("\"Ostatni\" stan licznika kilometrów: ");
    // System.out.println(lastMileage.readLast());

    Display display = new Display();

    display.print(DisplayConstants.LABEL_INPUT_CURRENT_MILEAGE);
    Integer currentMileage = typeMileage();
    if (currentMileage == null) {
      display.println(DisplayConstants.VALIDATION_INPUT_NULL_CURRENT_MILEAGE);
      return appError;
    }

    // display.println(DisplayConstants.LABEL_INPUT_CURRENT_MILEAGE +
    // currentMileage);
    Utils.br();
    saveDataToTextFile(currentMileage.toString());

    display.print(DisplayConstants.LABEL_INPUT_FUEL_AMOUNT);
    Double fuelAmount = Utils.enter.useLocale(Locale.GERMAN).nextDouble();
    Utils.br();
    display.println(DisplayConstants.MESSAGE_INFO_LAST_REFUELING_A + formattedDouble(fuelAmount)
        + DisplayConstants.MESSAGE_INFO_LAST_REFUELING_B);
    Utils.br();

    int previousMileage = Integer.parseInt(lastMileage);
    Double consumedFuel = deliverFuelConsumption(fuelAmount, currentMileage, previousMileage);
    display.println(DisplayConstants.MESSAGE_INFO_FUEL_USE_A + formattedDouble(consumedFuel)
        + DisplayConstants.MESSAGE_INFO_FUEL_USE_B);
    Utils.br();

    return getAppOk();
  }

  private Integer getLastMileageFromUser(String lastMileage, Integer tempFirstMileage) {
    if (lastMileage == null) {
      Utils.br(DisplayConstants.MESSAGE_INFO_NO_LAST_MILEAGE);
      Utils.br(DisplayConstants.LABEL_RADIO_ASK_FOR_LAST_MILEAGE);
      setReplyYes(Utils.checkState());
      if (isReplyYes()) {
        Utils.br(DisplayConstants.LABEL_INPUT_LAST_MILEAGE);
        return tempFirstMileage = typeMileage();
      } else {
        System.err.println(DisplayConstants.MESSAGE_ERROR_NO_LAST_MILEAGE);
        return null;
      }
    } else {
      Utils.br(DisplayConstants.MESSAGE_INFO_LAST_MILEAGE + lastMileage);
      return Integer.parseInt(lastMileage);
    }

  }

  private String lastMileage() {
    FileRead test = new FileRead();
    ArrayList<String> mileageListFromFile = test.readList();
    String lastMileage = getLastMileage(mileageListFromFile);
    return lastMileage;
  }

  private void saveDataToTextFile(String data) {
    if (data != null) {
      FileSave saveToFile = new FileSave();
      try {
        saveToFile.saveCurrentMileage(data);
      } catch (IOException ex) {
        Logger.getLogger(FileSave.class.getName()).log(Level.SEVERE, null, ex);
        System.out.println("File save error!");
      }
    } else {
      Utils.br("Couldn't save empty value to file!");
    }
  }

  private String formattedDouble(Double doubleToFormat) {
    DecimalFormat df = new DecimalFormat("#.00");
    return df.format(doubleToFormat);
  }

  private Double deliverFuelConsumption(Double fuelAmount, Integer currentMileage, Integer previousMileage) {
    if (previousMileage != 0) {
      Double result = (fuelAmount * 100) / (currentMileage - previousMileage);
      return result;
    } else {
      return null;
    }
  }

  private String getLastMileage(ArrayList<String> mileageListFromFile) {
    int mlffSize = mileageListFromFile.size();
    for (String element : mileageListFromFile) {
      int elementIndex = mileageListFromFile.indexOf(element);
      if (elementIndex + 1 == mlffSize) {
        return element;
      }
    }
    return null;
  }

  private Integer typeMileage() {
    try {
      Integer operation = Utils.enter.nextInt();
      return operation;
    } catch (InputMismatchException e) {
      Utils.br(DisplayConstants.MESSAGE_WARNING_MILEAGE_NOT_WHOLE_NUMBER);
      Utils.br(e);
    }
    return -1;
  }

  public void startApp(String mode) throws SQLException {
    if (mode.equals("db mode")) {

      Utils.br("Czy wyświetlić zawartość tabeli?");
      setReplyYes(Utils.checkState());
      if (isReplyYes()) {
        // Mileage mi = new Mileage();
        // System.out.println(mi.mileageMethodList());

        System.out.print(DisplayConstants.INFO_MESSAGE_TABLE);
        List<Mileage> mileages = prepareDataList();
        if (mileages.isEmpty()) {
          Utils.br(DisplayConstants.INFO_MESSAGE_EMPTY_TABLE);
        } else {
          Utils.br(countHeaderNames(prepareHeaderNamesArray()));
          Utils.br(prepareMileageHeaderRow());
          
          /**
           * Mileage table display formatter below:
           */
          // TODO
          // String separator = "\\|";
          // for (Mileage m : mileages) {
          // String tableRow = m.toString().trim();
          // String[] brokenList;
          // if(tableRow.startsWith("|")) {
          // tableRow = tableRow.substring(1).trim();
          // }
          // brokenList = breakString(tableRow, separator, true);
          // System.out.println(Arrays.toString(brokenList));
          // }

          // temporary displays as toString() method modified inside Mileage class:
          displayMileagesList(mileages);

          FuelConsumptionDb db1 = new FuelConsumptionDb();
          // I had to add throws declaration to startApp(String mode) method
          // It have to be fixed in future
          Double fuelConsumptionAvg = db1.selectAvgMileage(DbConstants.MILEAGE_FUEL_CONSUMPTION, DbConstants.MILEAGE_ID);
          Utils.br(averageFuelConsumptionMessage(fuelConsumptionAvg));

          Double fuelAmountAvg = db1.selectAvgMileage(DbConstants.MILEAGE_FUEL_AMOUNT, DbConstants.MILEAGE_ID);
          Utils.br(DisplayConstants.INFO_MESSAGE_AVG_FUEL_AMOUNT + Utils.formattedDouble(fuelAmountAvg, 1)
              + DisplayConstants.UNIT_LITER_L);

          Double nrOfKmAvg = db1.selectAvgMileage(DbConstants.MILEAGE_NR_OF_KM, DbConstants.MILEAGE_ID);
          StringBuilder averageDistanceMessage = new StringBuilder().append(DisplayConstants.INFO_MESSAGE_AVG_DISTANCE)
              .append(Utils.formattedDouble(nrOfKmAvg, 1)).append(DisplayConstants.UNIT_KILOMETER_KM);
          Utils.br(averageDistanceMessage);

          db1.closeConnection();
        }

      } else {
        Integer mileage = 0;
        Integer nrOfKm = 0;
        Double fuelAmount = 0.00;
        Double fuelConsumption = 0.00;

        FuelConsumptionDb db1 = new FuelConsumptionDb();
        Integer lastMileage = null;
        try {
          lastMileage = db1.selectLastMileage();
        } catch (SQLException e) {
          lastMileage = null;
        }
        db1.closeConnection();
        if (lastMileage == null) {
          Utils.br("Wygląda na to, że jest to pierwszy wpis do bazy danych.");
          lastMileage = 0;
          System.out.print("Aplikacja domyślnie przyjmuje, że poprzedni przebieg wynosi: " + lastMileage);
        } else {
          System.out.print("Poprzednio zarejestrowany przebieg: " + lastMileage);
        }
        Utils.br();
        Utils.br("Wprowadź wiersz danych;");
        do {
          System.out.print("Aktualny przebieg: ");
          mileage = typeMileage();
          Utils.br();
          if (lastMileage >= mileage) {
            Utils.br("Nowy przebieg musi być większy niż poprzedni!");
          }
        } while (lastMileage >= mileage);

        System.out.print("Ilość przejechanych kilometrów: ");
        nrOfKm = mileage - lastMileage;
        Utils.br(nrOfKm);
        System.out.print("Podaj ilość zatankowanego paliwa: ");
        fuelAmount = Utils.enter.useLocale(Locale.GERMAN).nextDouble();

        Utils.br();

        fuelConsumption = deliverFuelConsumption(fuelAmount, mileage, lastMileage);
        Utils.br("Zużycie paliwa: " + fuelConsumptionMessage(fuelConsumption) + " l/100km");
        Utils.br();

        Utils.br("Czy podane dane są prawidłowe?");
        Utils.br(mileage + "; " + nrOfKm + "; " + Utils.formattedDouble(fuelAmount) + "; "
            + fuelConsumptionMessage(fuelConsumption));
        setReplyYes(Utils.checkState());
        if (isReplyYes()) {
          FuelConsumptionDb db = new FuelConsumptionDb();
          if (db.insertMileage(mileage, nrOfKm, fuelAmount, fuelConsumption, Utils.createTimestamp())) {
            Utils.br("Dane zostały zapisane w bazie.");
          } else {
            System.err.println("Zapis nie powiódł się!");
          }
          db.closeConnection();
        } else {
          Utils.br("Zapis danych porzucony!");
        }
      }

    } else {
      ExtraMenu menu = new ExtraMenu();
      Utils.br(menu.enterCommand());

      Utils.br("Podaj nazwę tabeli do sprawdzenia: ");
      String tableName = Utils.enter.next();
      FuelConsumptionDb db23 = new FuelConsumptionDb();
      Integer test = db23.checkIfTableExists(tableName);
      if (test.equals(0)) {
        System.err.println("Tabela '" + tableName + "' nie istnieje!");
      } else if (test.equals(1)) {
        Utils.br("Tabela '" + tableName + "' istnieje :)");
      } else {
        System.err.println("Nieznany błąd!");
      }
      db23.closeConnection();
      Utils.br("Wprowadź nazwę tabeli do modyfikacji:");
      tableName = Utils.enter.next();
      Utils.br("Wprowadź nazwę kolumny do utworzenia:");
      String colName = Utils.enter.next();
      Utils.br("Wprowadź typ danych kolumny:");
      String colDataType = Utils.enter.next();
      Utils.br("Czy podane dane są prawidłowe?");
      Utils.br(tableName + "; " + colName + "; " + colDataType + "; ");
      setReplyYes(Utils.checkState());
      if (isReplyYes()) {
        FuelConsumptionDb db = new FuelConsumptionDb();

        if (db.addColumn(tableName, colName, colDataType)) {
          Utils.br("Dane zostały zapisane w bazie.");
        } else {
          System.err.println("Zapis nie powiódł się!");
        }
        db.closeConnection();
      } else {
        Utils.br("Zapis danych porzucony!");
      }
    }
  }

  private void displayMileagesList(List<Mileage> mileages) {
    for (Mileage m : mileages)
      Utils.br(m);
  }

  private String countHeaderNames(String[] prepareHeaderNamesArray) {
    List<Integer> headerNamesLengthList = new ArrayList<>();
    StringBuilder countResult = new StringBuilder();
    for (String headerName : prepareHeaderNamesArray) {
      Integer headerNameLength = headerName.length();
      countResult.append(headerNameLength).append(DisplayConstants.SPACE);
      headerNamesLengthList.add(headerNameLength);
    }
    return countResult.toString();
  }

  private String[] prepareHeaderNamesArray() {
    String str1 = DbConstants.MILEAGE_ID;
    String str2 = DbConstants.MILEAGE_MILEAGE;
    String str3 = DbConstants.MILEAGE_NR_OF_KM;
    String str4 = DbConstants.MILEAGE_FUEL_AMOUNT;
    String str5 = DbConstants.MILEAGE_FUEL_CONSUMPTION;
    String str6 = DbConstants.MILEAGE_DATE_INSERTED;
    String[] headerNamesArray = { str1, str2, str3, str4, str5, str6 };
    return headerNamesArray;
  }

  private String averageFuelConsumptionMessage(Double fuelConsumptionAvg) {
    StringBuilder averageFuelConsumptionMessage = new StringBuilder()
        .append(DisplayConstants.INFO_MESSAGE_AVG_FUEL_CONSUMPTION)
        .append(Utils.formattedDouble(fuelConsumptionAvg)).append(DisplayConstants.SPACE)
        .append(DisplayConstants.UNIT_LITER_L).append(DisplayConstants.SLASH).append(DisplayConstants.HUNDRED)
        .append(DisplayConstants.UNIT_KILOMETER_KM);
    return averageFuelConsumptionMessage.toString();
  }

  private String prepareMileageHeaderRow() {
    StringBuilder mileageHeaderRow = new StringBuilder();
    mileageHeaderRow.append(DisplayConstants.PIPE).append(DisplayConstants.SPACE).append(DbConstants.MILEAGE_ID)
        .append(DisplayConstants.SPACE).append(DisplayConstants.PIPE).append(DisplayConstants.SPACE)
        .append(DbConstants.MILEAGE_MILEAGE).append(DisplayConstants.SPACE).append(DisplayConstants.PIPE)
        .append(DisplayConstants.SPACE).append(DbConstants.MILEAGE_NR_OF_KM).append(DisplayConstants.SPACE)
        .append(DisplayConstants.PIPE).append(DisplayConstants.SPACE).append(DbConstants.MILEAGE_FUEL_AMOUNT)
        .append(DisplayConstants.SPACE).append(DisplayConstants.PIPE).append(DisplayConstants.SPACE)
        .append(DbConstants.MILEAGE_FUEL_CONSUMPTION).append(DisplayConstants.SPACE).append(DisplayConstants.PIPE)
        .append(DisplayConstants.SPACE).append(DbConstants.MILEAGE_DATE_INSERTED).append(Utils.temp10spaces)
        .append(DisplayConstants.SPACE).append(DisplayConstants.PIPE).append(DisplayConstants.SPACE);
    return mileageHeaderRow.toString();
    // 10 spaces above added temporary for better visual effect;
  }

  private List<Mileage> prepareDataList() {
    FuelConsumptionDb db = new FuelConsumptionDb();
    List<Mileage> dataList = db.selectMileage();
    db.closeConnection();
    return dataList;
  }

  private String fuelConsumptionMessage(Double fuelConsumption) {
    if (fuelConsumption != null) {
      return Utils.formattedDouble(fuelConsumption);
    } else {
      return "N/A";
    }

  }

  // private String[] breakString(String sourceString, String separator, boolean
  // dropSpaces) {
  // String resultList[] = null;
  // if (dropSpaces) {
  // sourceString = sourceString.replace(" ", "");
  // }
  // resultList = sourceString.split(separator);
  // return resultList;
  // }

}
