/**
 * 
 */
package dev.training.fuelconsumption;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

/**
 * @author artisticImpresion
 *
 */
public class Utils {

	public static Scanner enter = new Scanner(System.in);

	public static boolean checkState() {
		boolean isYes = false;
		boolean quit = false;
		while (!quit) {
			System.out.println("[T]ak/[N]ie?");
			char choice = Utils.enter.next().charAt(0);
			choice = charToUpperCase(choice);

			switch (choice) {
			case 'T':
				isYes = true;
				quit = true;
				break;
			case 'N':
				isYes = false;
				quit = true;
				break;
			default:
				System.out.println("Dokonaj wyboru!");
				break;
			}
		}
		return isYes;
	}

	public static char charToUpperCase(char charToConvert) {
		Character newChar = charToConvert;
		String newString = "";
		newString = newString + newChar;
		newString = StringUtils.capitalize(newString);
		char resultChar = newString.charAt(0);
		return resultChar;
	}

	public static void br() {
		System.out.println();
	}
	
	public static void br(Object param) {
    System.out.println(param.toString());
  }

	public static String formattedDouble(Double doubleToFormat) {
		DecimalFormat df = new DecimalFormat("0.00");
		return df.format(doubleToFormat);
	}

	public static String formattedDouble(Double doubleToFormat, int decimalPlace) {
		if (decimalPlace > 12) {
			decimalPlace = 12;
		}
		String dPl = "";
		String zero = "0";
		StringBuilder sB = new StringBuilder(dPl);
		for (int i = 0; i < decimalPlace; i++) {
			sB.append(zero);
		}
		String dfParam = "0." + sB;
		DecimalFormat df = new DecimalFormat(dfParam);
		return df.format(doubleToFormat);
	}
	
	public static String temp10spaces = "          ";
	
  public static Timestamp createTimestamp() {
    java.util.Date date = new Date();
    return new java.sql.Timestamp(date.getTime());
  }
  
  public static String formattedTimestamp(Timestamp timestampToFormat, String pattern) {
    return new SimpleDateFormat(pattern).format(timestampToFormat);
  }
	
}

  
