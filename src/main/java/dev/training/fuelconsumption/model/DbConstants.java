/**
 * 
 */
package dev.training.fuelconsumption.model;

/**
 * @author artisticImpresion
 *
 */
public class DbConstants {
  public static final String ERROR_MESSAGE_CLOSE_CONNECTION = "Problem z zamknięciem połączenia!";
  public static final String ERROR_MESSAGE_COLUMN_AVERAGE = "Nie udało się uzyskać średniej z kolumny: '%s'!";
  public static final String ERROR_MESSAGE_CREATING_TABLE = "Problem z utworzeniem tabeli!";
  public static final String ERROR_MESSAGE_NO_JDBC_DRIVER = "Brak sterownika JDBC!";
  public static final String ERROR_MESSAGE_LAST_MILEAGE = "Nie można uzyskać poprzedniego przebiegu!";
  public static final String ERROR_MESSAGE_OPENING_CONNECTION = "Problem z otwarciem połączenia!";
  public static final String ERROR_MESSAGE_TABLE_EXISTS = "Nie udało się uzyskać informacji nt. tabeli: '%s'.";
  public static final String MILEAGE_ID = "id_mileage";
	public static final String MILEAGE_FUEL_AMOUNT = "fuel_amount";
	public static final String MILEAGE_FUEL_CONSUMPTION = "fuel_consumption";
	public static final String MILEAGE_MILEAGE = "mileage";
	public static final String MILEAGE_NR_OF_KM = "number_of_km";
	public static final String MILEAGE_DATE_INSERTED = "date_inserted";
	public static final String MILEAGE_DATE_INSERTED_PATTERN = "yyyy-MM-dd HH:mm:ss";
	
}
