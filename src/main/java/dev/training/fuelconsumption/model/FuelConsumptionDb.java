/**
 * 
 */
package dev.training.fuelconsumption.model;

import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.LinkedList;
import java.sql.Timestamp;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author artisticImpresion
 *
 */
public class FuelConsumptionDb {
	private static final Logger LOG = Logger.getLogger(FuelConsumptionDb.class.getName());

	public static final String DRIVER = "org.sqlite.JDBC";
	public static final String DB_URL = "jdbc:sqlite:fuelconsumption.db";
	public static final String DB_URL2 = "jdbc:sqlite:test-second.db";

	private Connection connection;
	private Statement statement;
	private PreparedStatement ps = null;

	public FuelConsumptionDb() {
		try {
			Class.forName(FuelConsumptionDb.DRIVER);
		} catch (ClassNotFoundException e) {
			System.err.println(DbConstants.ERROR_MESSAGE_NO_JDBC_DRIVER);
			e.printStackTrace();
		}

		try {
		  //TODO
		  //temporary db changed from DB_URL to DB_URL2
		  //eventually it have to be implemented as part of application
		  //profiles feature
			connection = DriverManager.getConnection(DB_URL2);
			statement = connection.createStatement();
		} catch (SQLException e) {
			System.err.println(DbConstants.ERROR_MESSAGE_OPENING_CONNECTION);
			e.printStackTrace();
		}

		// try {
		// selectMileage();
		// } catch (Exception e) {
		createTables();
		// }

	}

	public boolean createTables() {
		String createMileage = "CREATE TABLE IF NOT EXISTS mileage (id_mileage INTEGER PRIMARY KEY AUTOINCREMENT, mileage int, number_of_km int, fuel_amount double, fuel_consumption double, date_inserted DATETIME)";

		try {
			statement.execute(createMileage);
		} catch (SQLException e) {
			System.err.println(DbConstants.ERROR_MESSAGE_CREATING_TABLE);
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean addColumn(String tableName, String colName, String colDataType) {
		String alterTable = String.format("ALTER TABLE %s ADD %s %s ", tableName, colName, colDataType); 
		Integer effect = null;
		try {
			ps  = connection.prepareStatement(alterTable);
			effect = ps.executeUpdate();
			System.out.println(effect + ". wierszy zmodyfikowano");
		} catch (Exception e) {
			//e.printStackTrace();
			LOG.log(Level.SEVERE, "Problem z dodaniem kolumny!", e);
			//System.err.println("Problem z dodaniem kolumny!");
			return false;
		}
		return true;
	}

	public boolean insertMileage(Integer currentMileage, Integer numberOfKm, Double fuelAmount,
			Double fuelConsumption, Timestamp ts) {
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement("INSERT INTO mileage VALUES (NULL, ?, ?, ?, ?, ?);");
			preparedStatement.setInt(1, currentMileage);
			preparedStatement.setInt(2, numberOfKm);
			preparedStatement.setDouble(3, fuelAmount);
//			if(fuelConsumption!=null) {
			  preparedStatement.setObject(4, fuelConsumption);
//			}
			preparedStatement.setTimestamp(5, ts);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Problem z zapisaniem wiersza danych!");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public List<Mileage> selectMileage() {
		List<Mileage> mileageList = new LinkedList<Mileage>();
		try {
			
//			ps = connection.prepareStatement("SELECT * FROM mileage");
//			
//			ResultSet result = ps;
			
			ResultSet result = statement.executeQuery("SELECT * FROM mileage");
			int id;
			Integer currentMileage;
			Integer numberOfKm;
			Double fuelAmount;
			Double fuelConsumption;
			Timestamp dateInserted;
			while (result.next()) {
				id = result.getInt(DbConstants.MILEAGE_ID);
				currentMileage = result.getInt(DbConstants.MILEAGE_MILEAGE);
				numberOfKm = result.getInt(DbConstants.MILEAGE_NR_OF_KM);
				fuelAmount = result.getDouble(DbConstants.MILEAGE_FUEL_AMOUNT);
				fuelConsumption = result.getDouble(DbConstants.MILEAGE_FUEL_CONSUMPTION);
				dateInserted = result.getTimestamp(DbConstants.MILEAGE_DATE_INSERTED);
				mileageList.add(new Mileage(id, currentMileage, numberOfKm, fuelAmount, fuelConsumption, dateInserted));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return mileageList;
	}

	public Integer selectLastMileage() throws SQLException {
		Integer currentMileage;
		try {
			ResultSet result = statement.executeQuery(
					"SELECT mileage FROM mileage WHERE id_mileage=" + "(SELECT max(id_mileage) FROM mileage)");
			currentMileage = result.getInt(DbConstants.MILEAGE_MILEAGE);
		} catch (SQLException e) {
			System.err.println(DbConstants.ERROR_MESSAGE_LAST_MILEAGE);
			throw new SQLException();
		}
		return currentMileage;
	}

	public Double selectAvgMileage(String colName, String tableIdColName) throws SQLException {
		Double average = 0.0d;
		try {
			ResultSet result = statement
					.executeQuery(String.format("SELECT avg(%s) FROM mileage WHERE %s > 1", colName, tableIdColName));
			while (result.next()) {
				average = result.getDouble(1);
			}
			return average;
		} catch (Exception e) {
			System.err.println(String.format(DbConstants.ERROR_MESSAGE_COLUMN_AVERAGE, colName));
			System.err.println(e.getMessage());
		}
		return null;
	}
	
	public Integer checkIfTableExists(String tableName) throws SQLException {
		Integer testFigure = null;
		try {
			ResultSet result = statement.executeQuery(String.format("SELECT count(*) FROM sqlite_master WHERE type='table' AND name='%s'", tableName)); 
			while (result.next()) {
				testFigure = Integer.parseInt(result.getString(1));
			}
			return testFigure;
		} catch (Exception e) {
			System.err.println(String.format(DbConstants.ERROR_MESSAGE_TABLE_EXISTS, tableName));
			System.err.println(e.getMessage());
			return null;
		}
		
	}
	
	public void closeConnection() {
		try {
			connection.close();
		} catch (SQLException e) {
			System.err.println(DbConstants.ERROR_MESSAGE_CLOSE_CONNECTION);
			e.printStackTrace();
		}
	}

}
