/**
 * 
 */
package dev.training.fuelconsumption.model;

import java.util.List;

/**
 * @author artisticImpresion
 *
 */
public class JdbcTest {
	public static void main(String[] args) {
		FuelConsumptionDb db = new FuelConsumptionDb();
		// db.insertMileage(0, 0, 0d);
		// db.insertMileage(320, 320, 7.34d);
		// db.insertMileage(655, 335, 7.25d);
		// db.insertMileage(1000, 345, 8.02d);
		// db.insertMileage(1333, 333, 7.33d);

		List<Mileage> mileages = db.selectMileage();

		System.out.println("[" + DbConstants.MILEAGE_ID + "] | " + DbConstants.MILEAGE_MILEAGE + " | "
				+ DbConstants.MILEAGE_NR_OF_KM + " | " + DbConstants.MILEAGE_FUEL_CONSUMPTION + " |");
		for (Mileage m : mileages)
			System.out.println(m);

		db.closeConnection();

	}
}
