/**
 * 
 */
package dev.training.fuelconsumption.model;

import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.ArrayList;

import dev.training.fuelconsumption.Utils;

/**
 * @author artisticImpresion
 *
 */
public class Mileage {
	private int id;
	private Integer currentMileage;
	private Integer numberOfKm;
	private Double fuelAmount;
	private Double fuelConsumption;
	private Timestamp dateInserted;
	private String consumedFuel;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getCurrentMileage() {
		return currentMileage;
	}

	public void setCurrentMileage(Integer currentMileage) {
		this.currentMileage = currentMileage;
	}

	public Integer getNumberOfKm() {
		return numberOfKm;
	}

	public void setNumberOfKm(Integer numberOfKm) {
		this.numberOfKm = numberOfKm;
	}

	public Double getFuelAmount() {
		return fuelAmount;
	}

	public void setFuelAmount(Double fuelAmount) {
		this.fuelAmount = fuelAmount;
	}

	public Double getFuelConsumption() {
		return fuelConsumption;
	}

	public void setFuelConsumption(Double fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}

	public Timestamp getDateInserted() {
		return dateInserted;
	}

	public void setDateInserted(Timestamp dateInserted) {
		this.dateInserted = dateInserted;
	}
	
	public String getConsumedFuel() {
	  return consumedFuel;
	}
	
	public void setConsumedFuel(String consumedFuel) {
	  this.consumedFuel = consumedFuel;
	}

	public Mileage() {
	}

	public Mileage(int id, Integer currentMileage, Integer numberOfKm, Double fuelAmount, Double fuelConsumption, Timestamp dateInserted) {
		this.id = id;
		this.currentMileage = currentMileage;
		this.numberOfKm = numberOfKm;
		this.fuelAmount = fuelAmount;
		this.fuelConsumption = fuelConsumption;
		this.dateInserted = dateInserted;
	}

	@Override
	public String toString() {
	  
		return "| " + addSpaces(DbConstants.MILEAGE_ID, ((Integer) id).toString()) + id + " | "
				+ addSpaces(DbConstants.MILEAGE_MILEAGE, currentMileage.toString()) + currentMileage + " | "
				+ addSpaces(DbConstants.MILEAGE_NR_OF_KM, numberOfKm.toString()) + numberOfKm + " | "
				+ addSpaces(DbConstants.MILEAGE_FUEL_AMOUNT, Utils.formattedDouble(fuelAmount))
				+ Utils.formattedDouble(fuelAmount) + " | "
				+ addSpaces(DbConstants.MILEAGE_FUEL_CONSUMPTION, consumedFuel())
				+ consumedFuel() + " | "
				+ addSpaces(DbConstants.MILEAGE_DATE_INSERTED + Utils.temp10spaces, Utils.formattedTimestamp(dateInserted, DbConstants.MILEAGE_DATE_INSERTED_PATTERN)) + Utils.formattedTimestamp(dateInserted, "yyyy-MM-dd HH:mm:ss") + " | ";
				//10 spaces above added temporary for better visual effect;
				
	}

	protected String consumedFuel() {
    if(fuelConsumption.equals(0.0)){
      setConsumedFuel("N/A");
      return getConsumedFuel();
    } else {
      setConsumedFuel(Utils.formattedDouble(fuelConsumption, 5).toString());
      return getConsumedFuel();
    }
  }

  public ArrayList<String> mileageMethodList() {
		ArrayList<String> mileageMethodList = new ArrayList<>();
		// Field[] fields = Mileage.class.getDeclaredFields();
		Method[] met = Mileage.class.getDeclaredMethods();
		for (Method f : met) {
			mileageMethodList.add(f.getName());
		}
		return mileageMethodList;
	}

	private String addSpaces(String colName, String toStringColValue) {
		int colNameLength = colName.length();
		int colValueLength = toStringColValue.length();
		int spacesToAdd = colNameLength - colValueLength;
		String space = "";
		for (int i = 0; i < spacesToAdd; i++) {
			space = space + " ";
		}
		return space;
	}
	private String addSpaces(String colName, Timestamp toStringColValue) {
		int colNameLength = colName.length();
		String tempString = "";
		try {
			tempString = toStringColValue.toString();
		} catch (Exception e) {
			tempString = "null";
		}
		int colValueLength = tempString.length();
		int spacesToAdd = colNameLength - colValueLength;
		String space = "";
		for (int i = 0; i < spacesToAdd; i++) {
			space = space + " ";
		}
		return space;
	}

}
